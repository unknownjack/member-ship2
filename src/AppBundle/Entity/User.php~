<?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
    use Doctrine\Common\Collections\ArrayCollection;

    /**
     * AuthenticationUser
     *
     * @ORM\Table(name="users")
     * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
     * @UniqueEntity(
     *     fields={"username"},
     *     message="This Username has already been used."
     * )
     */
    class User {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @var string
         *
         * @ORM\Column(
         *     name="username"
         * )
         * @Assert\Type("string")
         * @Assert\NotBlank(
         *     message="This Username should not be Empty!"
         * )
         **/
        private $username;

        /**
         * @ORM\Column(
         *     name="password",
         *     type="string"
         * )
         * @Assert\Type("string")
         * @Assert\NotBlank(
         *     message="This Password should not be Empty!"
         * )
         */
        private $password;

        /**
         * @Assert\Type("string")
         * @Assert\NotBlank(
         *     message="This Password should not be Empty!."
         * )
         */
        protected $passconf;

        /**
         * @ORM\Column(
         *     name="name",
         *     type="string"
         * )
         * @Assert\Type("string")
         * @Assert\NotBlank(
         *     message="This Surname should not be Empty!"
         * )
         */
        private $name;

        /**
         * @ORM\Column(
         *     name="lastname",
         *     type="string"
         * )
         * @Assert\Type("string")
         * @Assert\NotBlank(
         *     message="This Lastname should not be Empty!"
         * )
         */
        private $lastname;


        /**
         * @var Balance
         *
         * @ORM\OneToOne(targetEntity="AppBundle\Entity\Balance", mappedBy="user")
         */
        private $balance;

        /**
         * @var Friends
         *
         * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
         * @ORM\JoinTable(name="user_friends",
         *     joinColumns={
         *          @ORM\JoinColumn(name="user_id", referencedColumnName="id")
         *     },
         *     inverseJoinColumns={
         *          @ORM\JoinColumn(name="friends_id", referencedColumnName="id")
         *     }
         * )
         */
        private $friends;

        /**
         * @Assert\IsTrue(
         *     message="The Re-type Password was not the same!"
         * )
         */
        public function isPasswordValid() {
            if ($this->getPassword() === '') {
                return TRUE;
            }

            return $this->getPassword() == $this->getPassconf();
        }

        /**
         * @param int $id
         */
        public function setId($id) {
            $this->id = $id;
        }

        /**
         * Get id
         *
         * @return int
         */
        public function getId() {
            return $this->id;
        }

        /**
         * @return string
         */
        public function getUsername() {
            return $this->username;
        }

        /**
         * @return mixed
         */
        public function getPassword() {
            return $this->password;
        }

        /**
         * Set username
         *
         * @param string $username
         *
         * @return User
         */
        public function setUsername($username) {
            $this->username = $username;

            return $this;
        }

        /**
         * Set password
         *
         * @param string $password
         *
         * @return User
         */
        public function setPassword($password) {
            $this->password = $password;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getPassconf() {
            return $this->passconf;
        }

        /**
         * @param mixed $passconf
         */
        public function setPassconf($passconf) {
            $this->passconf = $passconf;
        }

        /**
         * Set name
         *
         * @param string $name
         *
         * @return User
         */
        public function setName($name) {
            $this->name = $name;

            return $this;
        }

        /**
         * Get name
         *
         * @return string
         */
        public function getName() {
            return $this->name;
        }

        /**
         * Set lastname
         *
         * @param string $lastname
         *
         * @return User
         */
        public function setLastname($lastname) {
            $this->lastname = $lastname;

            return $this;
        }

        /**
         * Get lastname
         *
         * @return string
         */
        public function getLastname() {
            return $this->lastname;
        }

        /**
         * Set balance
         *
         * @param $balance
         *
         * @return User
         */
        public function setBalance(Balance $balance = NULL) {
            $this->balance = $balance;

            return $this;
        }

        /**
         * Get balance
         *
         * @return Balance
         */
        public function getBalance() {
            return $this->balance;
        }

        /**
         * Constructor
         */
        public function __construct() {
            $this->friends = new ArrayCollection();
        }

        /**
         * Add friend
         *
         * @param Friend $friend
         *
         * @return User
         */
        public function addFriend(User $friend) {
            $this->friends[] = $friend;
            $friend->friends[] = $this;
        }

        /**
         * Remove friend
         *
         * @param Friend $friend
         */
        public function removeFriend(User $friend) {
            $this->friends->removeElement($friend);
            $friend->friends->removeElement($this);
        }

        /**
         * Get friends
         *
         * @return Collection
         */
        public function getFriends() {
            return $this->friends->toArray();
        }
    }
