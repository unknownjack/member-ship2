<?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * Balance
     *
     * @ORM\Table(name="balance")
     * @ORM\Entity(repositoryClass="AppBundle\Repository\BalanceRepository")
     */
    class Balance {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         **/
        private $id;


        /**
         * @var int
         *
         * @ORM\Column(name="balance_amount", type="float")
         **/
        private $amount;

        /**
         * @var int
         *
         * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="balance")
         * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
         **/
        private $user;

        public function getId() {
            return $this->id;
        }

        public function getAmount() {
            return $this->amount;
        }

        public function setAmount($amount) {
            $this->amount = $amount;
        }

        public function setUserId($userId) {
            $this->userId = $userId;

            return $this;
        }

        public function getUserId() {
            return $this->userId;
        }

        public function setUser(User $user = NULL) {
            $this->user = $user;

            return $this;
        }

        public function getUser() {
            return $this->user;
        }
    }
