<?php

    namespace AppBundle\Security\User;

    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Security\Core\User\EquatableInterface;

    class WebserviceUser implements UserInterface, EquatableInterface {
        private $id;
        private $username;
        private $password;
        private $name;
        private $lastname;
        private $roles;

        public function __construct($id, $username, $password, $name, $lastname, array $roles) {
            $this->id = $id;
            $this->username = $username;
            $this->password = $password;
            $this->name = $name;
            $this->lastname = $lastname;
            $this->roles = $roles;
        }

        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getRoles() {
            return $this->roles;
        }

        public function getPassword() {
            return $this->password;
        }

        public function getName() {
            return $this->name;
        }

        public function getLastname() {
            return $this->lastname;
        }

        public function getSalt() {
            return NULL;
        }

        public function getUsername() {
            return $this->username;
        }

        public function eraseCredentials() {
        }

        public function isEqualTo(UserInterface $user) {
            if (!$user instanceof WebserviceUser) {
                return FALSE;
            }

            if ($this->password !== $user->getPassword()) {
                return FALSE;
            }

            if ($this->username !== $user->getUsername()) {
                return FALSE;
            }

            if ($this->name !== $user->getName()) {
                return FALSE;
            }

            if ($this->lastname !== $user->getLastname()) {
                return FALSE;
            }

            return TRUE;
        }

    }