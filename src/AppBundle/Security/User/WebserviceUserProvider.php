<?php

    namespace AppBundle\Security\User;

    use AppBundle\Repository\UserRepository;
    use Symfony\Component\Security\Core\User\UserProviderInterface;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
    use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

    class WebserviceUserProvider implements UserProviderInterface {
        private $userEntityRepository;

        public function setUserEntityRepository(UserRepository $er){
            $this->userEntityRepository = $er;
        }

        public function loadUserByUsername($username) {
            $userData = $this->userEntityRepository->findUserBy($username);

            if ($userData) {
                $id = $userData->getId();
                $username = $userData->getUsername();
                $password = $userData->getPassword();
                $name = $userData->getName();
                $lastname = $userData->getLastname();

                return new WebserviceUser($id, $username, $password, $name, $lastname, ['ROLE_USER']);
            }

            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $username)
            );
        }

        public function refreshUser(UserInterface $user) {
            if (!$user instanceof WebserviceUser) {
                throw new UnsupportedUserException(
                    sprintf('Instances of "%s" are not supported.', get_class($user))
                );
            }

            return $this->loadUserByUsername($user->getUsername());
        }

        public function supportsClass($class) {
            return WebserviceUser::class === $class;
        }
    }