<?php
    /**
     * Created by PhpStorm.
     * User: mac
     * Date: 6/29/17
     * Time: 5:06 PM
     */

    namespace AppBundle\Service;

    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

    /**
     * Class FriendManager
     * @package AppBundle\Service
     */
    class FriendManager {
        private $em;
        private $auth;

        /**
         * FriendManager constructor.
         * @param TokenStorageInterface  $auth
         * @param EntityManagerInterface $em
         */
        public function __construct(TokenStorageInterface $auth, EntityManagerInterface $em) {
            $this->auth = $auth;
            $this->em = $em;
        }

        public function getAll() {
            $userID = $this->auth->getToken()->getUser()->getId();
            $user = $this->em->getRepository('AppBundle:User')->find($userID);

            return $user->getFriends();
        }

        public function getFriend($userID) {
            return $this->em->getRepository('AppBundle:User')
                ->find($userID);
        }

        public function addFriend($userID) {
            try {
                $friend = $this->em->getRepository('AppBundle:User')
                    ->find($userID);

                $authUserID = $this->auth->getToken()->getUser()->getId();
                $updateUser = $this->em->getRepository('AppBundle:User')
                    ->find($authUserID);

                $updateUser->addFriend($friend);

                $this->em->persist($updateUser);
                $this->em->flush();
            } catch (\Exception $e) {
                throw new \Exception('Cannot add friend exception!');
            }
        }

        public function removeFriend($friendID) {
            try {
                $friend = $this->em->getRepository('AppBundle:User')
                    ->find($friendID);

                $authUserID = $this->auth->getToken()->getUser()->getId();
                $updateUser = $this->em->getRepository('AppBundle:User')
                    ->find($authUserID);

                $updateUser->removeFriend($friend);

                $this->em->persist($updateUser);
                $this->em->flush();
            } catch (\Exception $e) {
                throw new \Exception('Cannot remove friend exception!');
            }
        }
    }