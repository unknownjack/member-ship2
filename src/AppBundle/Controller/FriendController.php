<?php

    namespace AppBundle\Controller;

    use AppBundle\Service\FriendManager;
    use Doctrine\ORM\EntityManagerInterface;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Session\Session;

    class FriendController extends Controller {
        /**
         * @Route("/friends", name="friend_index")
         **/
        public function indexAction(Session $session, EntityManagerInterface $em) {
            $isAddedFriend = $session->getFlashBag()->get('added_remove_friend', [NULL])[0];
            $addedFriendMsg = $session->getFlashBag()->get('added_remove_friend_msg', [NULL])[0];
            $userID = $this->getUser()->getId();
            $user = $em->getRepository('AppBundle:User')->find($userID);

            return $this->render('friend/index.html.twig', [
                'title'              => 'Friends',
                'friends'            => $user->getFriends(),
                'isAddFriendSuccess' => $isAddedFriend,
                'addFriendMsg'       => $addedFriendMsg
            ]);
        }

        /**
         * @Route("/friends/{user_id}/add", name="friend_add", requirements={"user_id": "\d+"})
         * @Method({"GET"})
         **/
        public function addFriendAction($user_id, FriendManager $friendManager, Session $session) {
            try {
                $friendManager->addFriend($user_id);
                $session->getFlashBag()->add('added_remove_friend', TRUE);
                $session->getFlashBag()->add('added_remove_friend_msg', 'Friend added successfully');
            } catch (\Exception $e) {
                $session->getFlashBag()->add('added_remove_friend', FALSE);
                $session->getFlashBag()->add('added_remove_friend_msg', 'There is an errors during adding friend.');
            }

            return $this->redirectToRoute('friend_index');
        }

        /**
         * @Route("/friends/{user_id}/remove", name="friend_remove", requirements={"user_id": "\d+"})
         * @Method({"GET"})
         */
        public function removeFriendAction($user_id, FriendManager $friendManager, Session $session) {
            try {
                $friendManager->removeFriend($user_id);
                $session->getFlashBag()->add('added_remove_friend', TRUE);
                $session->getFlashBag()->add('added_remove_friend_msg', 'Friend removed successfully');
            } catch (\Exception $e) {
                $session->getFlashBag()->add('added_remove_friend', FALSE);
                $session->getFlashBag()->add('added_remove_friend_msg', 'There is an errors during removing friend.');
            }

            return $this->redirectToRoute('friend_index');
        }

        /**
         * @Route("/friends/{user_id}", name="friend_detail", requirements={"user_id": "\d+"})
         */
        public function detailAction($user_id, FriendManager $friendManager) {
            return $this->render('friend/detail.html.twig', [
                'title'  => 'Friends Detail',
                'friend' => $friendManager->getFriend($user_id)
            ]);
        }
    }
