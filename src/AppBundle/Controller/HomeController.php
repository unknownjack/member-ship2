<?php

    namespace AppBundle\Controller;

    use Doctrine\ORM\EntityManagerInterface;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    class HomeController extends Controller {
        /**
         * @Route("/", name="homepage")
         */
        public function indexAction(EntityManagerInterface $em) {
            $userID = $this->get('security.token_storage')
                ->getToken()
                ->getUser()
                ->getId();
            $user = $em->getRepository('AppBundle:User')
                ->find($userID);

            return $this->render('default/index.html.twig', [
                'title'    => 'Home',
                'user'     => $user,
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            ]);
        }
    }
