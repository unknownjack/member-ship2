<?php

    namespace AppBundle\Controller;

    use AppBundle\Entity\Balance;
    use AppBundle\Entity\User;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\Config\Definition\Exception\Exception;
    use Symfony\Component\HttpFoundation\Request;

    class UserController extends Controller {
        /**
         * @Route("/users", name="user_index")
         */
        public function indexAction(EntityManagerInterface $em) {
            $authUser = $this->get('security.token_storage')->getToken()->getUser();
            $users = $em->getRepository('AppBundle:User')->findAllNotFriendOf($authUser->getId());
//            dump($users);die();

            return $this->render('user/index.html.twig', [
                'title' => 'User',
                'users' => $users
            ]);
        }

        /**
         * @Route("/users/register", name="user_register")
         */
        public function registerAction(Request $req, EntityManagerInterface $em) {
            $errors = [];

            if ($req->get('submit') === 'register') {
                $balance = new Balance();
                $balance->setAmount(5000); // Initialize money for new User.

                $user = new User();
                $user->setUsername($req->get('username'));
                $user->setPassword($req->get('password'));
                $user->setPassconf($req->get('passconf'));
                $user->setName($req->get('name'));
                $user->setLastname($req->get('lastname'));

                $balance->setUser($user);

                $validator = $this->get('validator');
                $errors = $validator->validate($user);

                if (count($errors) > 0) {
                    $transformedError = [];

                    foreach ($errors as $err) {
                        $transformedError[$err->getPropertyPath()] = $err->getMessage();
                    }

                    $errors = $transformedError;
                } else {
                    try {
                        $em->persist($balance);
                        $em->persist($user);
                        $em->flush();

                        return $this->redirectToRoute('security_login');
                    } catch (Exception $e) {
                        $errors = ['register' => 'There is an error occur during Registration!'];
                    }
                }
            }

            return $this->render('user/register.html.twig', [
                'title'  => 'Registration',
                'errors' => $errors
            ]);
        }
    }
