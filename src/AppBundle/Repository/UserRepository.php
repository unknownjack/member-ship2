<?php

    namespace AppBundle\Repository;

    use Doctrine\ORM\EntityRepository;

    class UserRepository extends EntityRepository {

        public function findUserBy($username) {
            return $this->getEntityManager()
                ->getRepository('AppBundle\Entity\User')
                ->findOneByUsername($username);
        }

        public function findAllUserWithout($userID = 0) {
            return $this->getEntityManager()
                ->getRepository('AppBundle\Entity\User')
                ->createQueryBuilder('u')
                ->where('u.id != :USER_ID')
                ->setParameter('USER_ID', $userID)
                ->getQuery()
                ->getResult();
        }

        public function findAllNotFriendOf($userID) {
            $user = $this->getEntityManager()
                ->getRepository('AppBundle:User')
                ->find($userID);

            return $this->getEntityManager()
                ->getRepository('AppBundle:User')
                ->createQueryBuilder('u')
                ->where(':USER NOT MEMBER OF u.friends')
                ->andWhere('u.id <> :USER_ID')
                ->setParameter('USER', $user)
                ->setParameter('USER_ID', $userID)
                ->getQuery()
                ->getResult();
        }
    }